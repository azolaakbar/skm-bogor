<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvPertanyaanTipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('srv_pertanyaan_tipe', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nama');
        $table->boolean('is_deleted');
      });

      DB::insert("INSERT INTO srv_pertanyaan_tipe(nama) VALUES('Pilihan Ganda');");
      DB::insert("INSERT INTO srv_pertanyaan_tipe(nama) VALUES('Bintang');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
