<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnTimestampSrvGrupBobot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('srv_grup_bobot', function (Blueprint $table) {
        $table->dropColumn(['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
