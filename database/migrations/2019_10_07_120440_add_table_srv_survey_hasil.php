<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSrvSurveyHasil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('srv_survey_hasil', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('id_grup');
        $table->integer('jml_bobot');
        $table->string('nama');
        $table->string('alamat');
        $table->integer('usia');
        $table->enum('jk', ['L', 'P']);
        $table->integer('id_pendidikan');
        $table->integer('id_pekerjaan');
        $table->string('email');
        $table->dateTime('created_at')->nullable();
        $table->dateTime('deleted_at')->nullable();
        $table->integer('deleted_by');
        $table->boolean('is_deleted');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
