<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class AddGuidToPertanyaanTipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("TRUNCATE TABLE srv_pertanyaan_tipe;");
        Schema::table('srv_pertanyaan_tipe', function (Blueprint $table) {
            $table->char('guid', 36)->unique();
        });


        DB::insert("INSERT INTO srv_pertanyaan_tipe(nama,guid) VALUES('Pilihan Ganda','". Str::uuid() ."');");
        DB::insert("INSERT INTO srv_pertanyaan_tipe(nama,guid) VALUES('Bintang','". Str::uuid() ."');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('srv_pertanyaan_tipe', function (Blueprint $table) {
            //
        });
    }
}
