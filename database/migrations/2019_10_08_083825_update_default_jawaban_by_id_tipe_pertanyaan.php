<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDefaultJawabanByIdTipePertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $default_jawaban = '{"1":{"A":{"label":"Sangat Baik","bobot":4},"B":{"label":"Baik","bobot":3},"C":{"label":"Kurang Baik","bobot":2},"D":{"label":"Buruk","bobot":1}},"2":{"*":{"label":"*","bobot":"1"},"**":{"label":"**","bobot":"2"},"***":{"label":"***","bobot":"3"},"****":{"label":"****","bobot":"4"}}}';
        DB::update("UPDATE mst_konfigurasi SET default_jawaban='{$default_jawaban}'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
