<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingDefaultJawabanStuffOnKonfigurasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_konfigurasi', function (Blueprint $table) {
            $table->string("default_pertanyaan_tipe_selected")->nullable();
            $table->tinyInteger("default_pertanyaan_tipe_hide")->nullable()->default(0);
        });

        $default_jawaban = '{"opsional":{"A":{"label":"Sangat Baik","bobot":4},"B":{"label":"Baik","bobot":3},"C":{"label":"Kurang Baik","bobot":2},"D":{"label":"Buruk","bobot":1}},"bintang":{"*":{"label":"*","bobot":"1"},"**":{"label":"**","bobot":"2"},"***":{"label":"***","bobot":"3"},"****":{"label":"****","bobot":"4"},"*****":{"label":"*****","bobot":"5"}}}';
        DB::update("UPDATE mst_konfigurasi SET default_jawaban='{$default_jawaban}', default_pertanyaan_tipe_selected=2,default_pertanyaan_tipe_hide=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_konfigurasi', function (Blueprint $table) {
            //
        });
    }
}
