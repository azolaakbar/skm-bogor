<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePertanyaanUnsur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('srv_pertanyaan_unsur', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->boolean('is_deleted')->default(0);
        });

        DB::insert("INSERT INTO srv_pertanyaan_unsur(nama) VALUES('Persyaratan'),('Sistem, Mekanisme dan Prosedur'),('Waktu Penyelasaian'),('Biaya / Tarif'),('Produk Spesifikasi Jenis Pelayanan'),('Kompetensi Pelaksana'),('Perilaku Pelaksana'),('Penanganan Pengaduan'),('Sarana')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('srv_pertanyaan_unsur');
    }
}
