<?php

use Illuminate\Database\Seeder;

class PekerjaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('mst_pekerjaan')->insert([
        'nama' => 'PNS',
      ]);

      DB::table('mst_pekerjaan')->insert([
        'nama' => 'TNI',
      ]);

      DB::table('mst_pekerjaan')->insert([
        'nama' => 'POLRI',
      ]);

      DB::table('mst_pekerjaan')->insert([
        'nama' => 'SWASTA',
      ]);

      DB::table('mst_pekerjaan')->insert([
        'nama' => 'WIRAUSAHA',
      ]);

      DB::table('mst_pekerjaan')->insert([
        'nama' => 'LAINNYA',
      ]);
    }
}
