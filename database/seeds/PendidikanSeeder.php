<?php

use Illuminate\Database\Seeder;

class PendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('mst_pendidikan')->insert([
        'nama' => 'SD',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'SMP',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'SMA/K',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'D1',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'D2',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'D3',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'S1',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'S2',
      ]);

      DB::table('mst_pendidikan')->insert([
        'nama' => 'S3',
      ]);
    }
}
