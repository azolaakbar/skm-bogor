<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konfigurasi extends Model
{
  protected $table = "mst_konfigurasi";
  public $timestamps = false;

  protected $primaryKey = null;
  public $incrementing = false;
}
