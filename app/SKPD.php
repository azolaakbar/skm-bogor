<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SKPD extends Model
{
  protected $table = "mst_skpd";
  protected $primaryKey = "id";
  public $timestamps = false;
}
