<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class PertanyaanTipe extends Model
{
  protected $table = "srv_pertanyaan_tipe";
  public $timestamps = false;
}
