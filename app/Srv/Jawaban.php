<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
  protected $table = "srv_jawaban";
  public $timestamps = false;
}
