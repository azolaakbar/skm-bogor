<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class SurveyHasil extends Model
{
  protected $table = "srv_survey_hasil";
  public $timestamps = false;
}
