<?php

namespace App\Srv;

use Illuminate\Database\Eloquent\Model;

class GrupBobot extends Model
{
  protected $table = "srv_grup_bobot";
  public $timestamps = false;
}
