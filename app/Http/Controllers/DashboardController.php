<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Peminjaman;
use App\Simpanan;
use App\BayarPertama;
use App\Konfigurasi;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      /*Untuk admin*/
      return view('home');
    }

    public function tentang()
    {
      $konfigurasi = konfigurasi::first();

      return view('tentang', [
        'data'=> $konfigurasi
      ]);
    }

    public function skema()
    {
      $konfigurasi = konfigurasi::first();

      return view('skema', [
        'data'=> $konfigurasi
      ]);
    }
}
