<?php

namespace App\Http\Controllers;

Use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Pendidikan;
use App\Pekerjaan;
use App\Srv\Grup;
use App\Srv\Pertanyaan;
use App\Srv\Jawaban;
use App\Srv\SurveyHasil;
use App\Srv\SurveyHasilDetail;
use App\Srv\Penilaian;

date_default_timezone_set('Asia/Jakarta');

class LaporanController extends Controller
{
    public function srv_tahunan(Request $request)
    {
      $skpd = get_skpd_by_login();
      $survey = Grup::where('is_deleted', 0)->get();
      $hit_survey_bulanan = [];
      $selected_survey = [];
      $total_survey_tahunan = "";
      $total_bobot_per_unsur = [];
      $penilaian = [];
      $penilaian['nilai_interval_konversi'] = 0;
      $penilaian['show_bintang'] = 0;

      // jika sudah submit
      if($request->input('skpd') > 0){
        $selected_survey = Grup::findOrfail($request->input('survey'));

        /* START total pengisian survey tahunan */
        $total_survey_tahunan = DB::table('srv_survey_hasil AS hs')
        ->select(DB::raw("hs.id"))
        ->join('srv_grup AS g', 'g.id', '=', 'hs.id_grup')
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('hs.created_at', $request->input('tahun'))
        ->where('g.id', $request->input('survey'))
        ->where('hs.is_deleted', 0)
        ->get()->count();
        /* END */

        /* START Hit survey per bulan */
        $hasil_survey = DB::select(DB::raw(
          "SELECT MONTH(hs.created_at) AS bulan, CAST((SUM(hs.jml_bobot)/SUM(hs.max_bobot)*4) AS DECIMAL(12,2)) AS hit from srv_survey_hasil AS hs
          JOIN srv_grup AS g
          	ON g.id = hs.id_grup
          WHERE hs.is_deleted = 0 AND
          g.id = {$request->input('survey')} AND
          g.id_skpd = {$request->input('skpd')} AND
          YEAR(hs.created_at) = {$request->input('tahun')}
          GROUP BY month(hs.created_at)"
        ));

        foreach($hasil_survey AS $idx => $row){
          $hit_survey_bulanan[$row->bulan] = $row->hit;
        }

        for($i=1; $i<=12; $i++){
          $hit_survey_bulanan[$i] = isset($hit_survey_bulanan[$i]) ? $hit_survey_bulanan[$i] : 0;
        }
        //$hit_survey_bulanan
        /* END */

        /* START Score per unsur pertanyaan*/
        $total_bobot_per_unsur = DB::table('srv_pertanyaan AS p')
        ->select(DB::raw("up.id AS id_unsur, up.nama AS unsur, ((SUM(j.bobot)/SUM(mb.max_bobot))*4) AS score"))
        ->join('srv_pertanyaan_unsur AS up', 'p.id_pertanyaan_unsur', '=', 'up.id')
        ->join('srv_jawaban AS j', 'j.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup_pertanyaan AS gp', 'gp.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
        ->join('srv_survey_hasil_detail AS shd', 'shd.id_jawaban', '=', 'j.id')
        ->join('srv_survey_hasil AS sh', function($join) {
          $join->on('sh.id', '=', 'shd.id_hasil')
          ->on('sh.id_grup', '=', 'g.id');
        })
        ->join(DB::raw("(SELECT 
          MAX(bobot) max_bobot, jb.id_pertanyaan
          FROM srv_jawaban jb 
          INNER JOIN srv_pertanyaan pb ON jb.id_pertanyaan = pb.id
          INNER JOIN srv_grup_pertanyaan gp ON gp.id_pertanyaan = pb.id
          WHERE gp.id_grup = '{$request->input('survey')}'
          GROUP BY jb.id_pertanyaan) AS mb"), 'mb.id_pertanyaan', '=', 'shd.id_pertanyaan')
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->where('p.is_deleted', 0)
        ->where('up.is_deleted', 0)
        ->where('j.is_deleted', 0)
        ->where('gp.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('sh.is_deleted', 0)
        ->where('shd.is_deleted', 0)
        ->groupBy('g.id')
        ->groupBy('up.id')
        ->orderBy('up.id', 'ASC')
        ->get();
        /* END */

        /* START IKM rumus => SUM(jml_bobot)/SUM(max_bobot)*100 */
        $q_hs = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot'))
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->get()
        ->first();

        if(isset($q_hs->sum_jml_bobot)){
          $ikm = ($q_hs->sum_jml_bobot / $q_hs->sum_max_bobot) * 4;
          $ikm = round($ikm, 2);
          $penilaian['nilai_interval_konversi'] = $ikm;
          $penilaian['show_bintang'] = $ikm ;
        }
        /* END */
      }

      return view('laporan.srv_tahunan', [
        'skpd' => $skpd,
        'survey' => $survey,
        'request' => $request->input(),
        'hit_survey_bulanan' => $hit_survey_bulanan,
        'total_survey_tahunan' => $total_survey_tahunan,
        'selected_survey' => $selected_survey,
        'total_bobot_per_unsur' => $total_bobot_per_unsur,
        'penilaian' => $penilaian
      ]);
    }


    public function srv_bulanan(Request $request)
    {
      $skpd = get_skpd_by_login();
      $survey = Grup::where('is_deleted', 0)->get();
      $hit_survey_bulanan = [];
      $selected_survey = [];
      $total_survey_tahunan = "";
      $total_bobot_per_unsur = [];
      $kritiksaran = [];
      $penilaian = [];
      $penilaian['nilai_interval_konversi'] = 0;
      $penilaian['show_bintang'] = 0;

      // jika sudah submit
      if($request->input('skpd') > 0){
        $selected_survey = Grup::findOrfail($request->input('survey'));
        $bulan = $request->input('bulan')/1;

        /* START total pengisian survey tahunan */
        $total_survey_tahunan = DB::table('srv_survey_hasil AS hs')
        ->select(DB::raw("hs.id"))
        ->join('srv_grup AS g', 'g.id', '=', 'hs.id_grup')
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('hs.created_at', $request->input('tahun'))
        ->whereMonth('hs.created_at', $bulan)
        ->where('g.id', $request->input('survey'))
        ->where('hs.is_deleted', 0)
        ->get()->count();
        /* END */

        /* START Hit survey per bulan */
        $hasil_survey = DB::select(DB::raw(
          "SELECT DAY(hs.created_at) AS tanggal, CAST((SUM(hs.jml_bobot)/SUM(hs.max_bobot)*4) AS DECIMAL(12,2)) AS hit from srv_survey_hasil AS hs
          JOIN srv_grup AS g
            ON g.id = hs.id_grup
          WHERE hs.is_deleted = 0 AND
          g.id = {$request->input('survey')} AND
          g.id_skpd = {$request->input('skpd')} AND
          YEAR(hs.created_at) = {$request->input('tahun')} AND
          MONTH(hs.created_at) = {$bulan}
          GROUP BY DAY(hs.created_at)"
        ));

        foreach($hasil_survey AS $idx => $row){
          $hit_survey_bulanan[$row->tanggal] = $row->hit;
        }

        for($i=1; $i<=12; $i++){
          $hit_survey_bulanan[$i] = isset($hit_survey_bulanan[$i]) ? $hit_survey_bulanan[$i] : 0;
        }
        //$hit_survey_bulanan
        /* END */

        /* START Score per unsur pertanyaan*/
        $total_bobot_per_unsur = DB::table('srv_pertanyaan AS p')
        ->select(DB::raw("up.id AS id_unsur, up.nama AS unsur, ((SUM(j.bobot)/SUM(mb.max_bobot))*4) AS score"))
        ->join('srv_pertanyaan_unsur AS up', 'p.id_pertanyaan_unsur', '=', 'up.id')
        ->join('srv_jawaban AS j', 'j.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup_pertanyaan AS gp', 'gp.id_pertanyaan', '=', 'p.id')
        ->join('srv_grup AS g', 'g.id', '=', 'gp.id_grup')
        ->join('srv_survey_hasil_detail AS shd', 'shd.id_jawaban', '=', 'j.id')
        ->join('srv_survey_hasil AS sh', function($join) {
          $join->on('sh.id', '=', 'shd.id_hasil')
          ->on('sh.id_grup', '=', 'g.id');
        })
        ->join(DB::raw("(SELECT 
          MAX(bobot) max_bobot, jb.id_pertanyaan
          FROM srv_jawaban jb 
          INNER JOIN srv_pertanyaan pb ON jb.id_pertanyaan = pb.id
          INNER JOIN srv_grup_pertanyaan gp ON gp.id_pertanyaan = pb.id
          WHERE gp.id_grup = '{$request->input('survey')}'
          GROUP BY jb.id_pertanyaan) AS mb"), 'mb.id_pertanyaan', '=', 'shd.id_pertanyaan')
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        ->where('p.is_deleted', 0)
        ->where('up.is_deleted', 0)
        ->where('j.is_deleted', 0)
        ->where('gp.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('sh.is_deleted', 0)
        ->where('shd.is_deleted', 0)
        ->groupBy('g.id')
        ->groupBy('up.id')
        ->orderBy('up.id', 'ASC')
        ->get();
        /* END */

        /* START IKM rumus => SUM(jml_bobot)/SUM(max_bobot)*100 */
        $q_hs = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot'))
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id', $request->input('survey'))
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        ->get()
        ->first();

        if(isset($q_hs->sum_jml_bobot)){
          $ikm = ($q_hs->sum_jml_bobot / $q_hs->sum_max_bobot) * 4;
          $ikm = round($ikm, 2);
          $penilaian['nilai_interval_konversi'] = $ikm;
          $penilaian['show_bintang'] = $ikm ;
        }

        $kritiksaran = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('sh.kritik_saran, sh.created_at, sh.nama'))
        ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
        ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
        ->where('sh.is_deleted', 0)
        ->where("sh.id_grup", $request->input("survey"))
        ->whereRaw(DB::raw("(sh.kritik_saran IS NOT NULL AND sh.kritik_saran != '')"))
        ->orderBy('sh.id', 'ASC')
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->whereMonth('sh.created_at', $bulan)
        ->get();

        /* END */
      }

      return view('laporan.srv_bulanan', [
        'skpd' => $skpd,
        'survey' => $survey,
        'request' => $request->input(),
        'hit_survey_bulanan' => $hit_survey_bulanan,
        'total_survey_tahunan' => $total_survey_tahunan,
        'selected_survey' => $selected_survey,
        'total_bobot_per_unsur' => $total_bobot_per_unsur,
        'penilaian' => $penilaian,
        'kritiksaran' => $kritiksaran
      ]);
    }

    public function srv_bulanan_tahun(Request $request, $id_skpd)
    {
        $tahun = Grup::where("is_deleted", 0)->select("tahun")->where("id_skpd", $id_skpd)
        ->groupBy("tahun")
        ->orderBy("tahun")
        ->get();

        return response()->json($tahun);
    }

    public function get_pensurvey(Request $request, $id_grup){

      $bulan = $request->input("bulan")/1;
      $data = DB::table('srv_survey_hasil AS sh')
      ->select(DB::raw('sh.nama, sh.alamat, sh.usia, sh.jk, p.nama AS pendidikan, pj.nama AS pekerjaan, sh.email'))
      ->join("mst_pendidikan AS p", "p.id","=","sh.id_pendidikan")
      ->join("mst_pekerjaan AS pj", "pj.id","=","sh.id_pekerjaan")
      ->where('sh.is_deleted', 0)
      ->where("sh.id_grup", $id_grup)
      ->whereYear('sh.created_at', $request->input('tahun'))
      ->whereMonth('sh.created_at', $bulan)
      ->orderBy('sh.id', 'ASC');

      return Datatables::of($data)->make(true);
    }

    public function ikm(Request $request)
    {
      $skpd = get_skpd_by_login();
      $ikm_tahunan_asc = [];
      $ikm_tahunan_desc = [];

      // jika sudah submit
      if($request->input('skpd') > 0){
        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikm_tahunan_asc = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('g.id, g.nama AS survey, SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot,
        (ROUND(SUM(sh.jml_bobot) / SUM(sh.max_bobot)*4, 2)) AS ikm')) //
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->groupBy('g.id')
        ->orderBy('ikm', 'ASC')
        ->limit(20)
        ->get();

        $ikm_tahunan_desc = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('g.id, g.nama AS survey, SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot,
        (ROUND(SUM(sh.jml_bobot) / SUM(sh.max_bobot)*4, 2)) AS ikm')) //
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->where('g.id_skpd', $request->input('skpd'))
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->groupBy('g.id')
        ->orderBy('ikm', 'DESC')
        ->limit(20)
        ->get();
      }

      // dd($ikm_tahunan_asc);

      return view('laporan.ikm', [
        'skpd' => $skpd,
        'request' => $request->input(),
        'ikm_tahunan_asc' => $ikm_tahunan_asc,
        'ikm_tahunan_desc' => $ikm_tahunan_desc
      ]);
    }

    public function ikm_tingkat_kota(Request $request)
    {
      // $skpd = get_skpd_by_login();
      $option_tahun = Grup::select('tahun')->where('is_deleted', 0)->groupBy('tahun')->orderBy('tahun', 'ASC')->get();
      $ikm_tingkat_kota = [];

      // jika sudah submit
      if($request->input('tahun') > 0){
        /* rumus => SUM(jml_bobot)/SUM(max_bobot)*100  */
        $ikm_tingkat_kota = DB::table('srv_survey_hasil AS sh')
        ->select(DB::raw('g.id_skpd, skpd.name AS skpd_name, SUM(sh.jml_bobot) AS sum_jml_bobot, SUM(sh.max_bobot) AS sum_max_bobot,
        (ROUND(SUM(sh.jml_bobot) / SUM(sh.max_bobot)*4, 2)) AS ikm')) //
        ->join('srv_grup AS g', 'g.id', '=', 'sh.id_grup')
        ->join('mst_skpd AS skpd', 'skpd.id', '=', 'g.id_skpd')
        ->where('sh.is_deleted', 0)
        ->where('g.is_deleted', 0)
        ->whereYear('sh.created_at', $request->input('tahun'))
        ->groupBy('g.id_skpd')
        ->orderBy('g.id_skpd', 'ASC')
        ->get();
      }

      return view('laporan.ikm_tingkat_kota', [
        'option_tahun' => $option_tahun,
        'request' => $request->input(),
        'ikm_tingkat_kota' => $ikm_tingkat_kota
      ]);
    }

}
