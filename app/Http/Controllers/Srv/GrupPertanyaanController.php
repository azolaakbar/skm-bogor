<?php

namespace App\Http\Controllers\Srv;

Use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Validator;
use Auth;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use App\Srv\Grup;
use App\Srv\GrupBobot;
use App\Srv\GrupPertanyaan;
use App\Srv\Pertanyaan;
use App\Srv\PertanyaanTipe;
use App\Konfigurasi;
use App\Srv\PertanyaanUnsur;
use App\Srv\Penilaian;
use App\Srv\Jawaban;

date_default_timezone_set('Asia/Jakarta');

class GrupPertanyaanController extends Controller
{
    public function id_skpd_by_login()
    {
      $id = get_skpd_by_login()->map(function($val,$idx) { return $val->id; });
      return $id; // array
    }

    public function index()
    {
      $konfigurasi = Konfigurasi::first();
      return view('srv.grup_pertanyaan-list',[
        'konfigurasi' => $konfigurasi
      ]);
    }

    public function create()
    {
      $skpd = get_skpd_by_login();
      $pertanyaan_tipe = PertanyaanTipe::where('is_deleted', 0)->get();
      $konfigurasi = Konfigurasi::first();
      $pertanyaan_unsur = PertanyaanUnsur::where("is_deleted", 0)->get();

      return view('srv.grup_pertanyaan-form', [
        'skpd' => $skpd,
        'pertanyaan_tipe' => $pertanyaan_tipe,
        'pertanyaan_unsur' => $pertanyaan_unsur,
        'konfigurasi' => $konfigurasi,
        'penilaian' => Penilaian::get()
      ]);
    }

    public function store(Request $request)
    {
      $logged_user = Auth::user();

      // dd($request->input('custom_pertanyaan'));


      $validator = Validator::make($request->all(), [
        'skpd' => 'required',
        'judul' => 'required',
        'bobot_start.*' => 'required|numeric',
        'bobot_end.*' => 'required|numeric',
        'nama' => 'required', // ini adalah label Penilaian
        'id_pertanyaan.*' => 'required',
        'slug' => [
          'required',
          Rule::unique('srv_grup', 'slug')->where(function ($query){
            return $query->where('is_deleted', 0);
          })
        ],
        'tahun' => 'required|numeric',
      ],[
        'skpd.required' => 'Unit Layanan harus diisi!',
        'judul.required' => 'Judul Survey harus diisi!',
        'bobot_start.required' => 'Range Nilai harus diisi!',
        'bobot_start.numeric' => 'Range Nilai harus diisi angka!',
        'bobot_end.required' => 'Range Nilai harus diisi!',
        'bobot_end.numeric' => 'Range Nilai harus diisi angka!',
        'nama.required' => 'Label harus diisi!',
        'id_pertanyaan.required' => 'Pertanyaan harus diisi!',
        'tahun.required' => 'Tahun harus diisi!',
        'tahun.numeric' => 'Tahun harus diisi angka!',
      ]);

      if($validator->fails()){

        return back()
              ->withErrors($validator)
              ->withInput();
      }

      $id_tipe_pertanyaan = PertanyaanTipe::where('guid', $request->input('tipe_pertanyaan'))->get()->first();

      //check if tipe pertanyaan is exist?
      //make sure id pertanyaan is exist
      $id_tipe_pertanyaan = isset($id_tipe_pertanyaan->id) ? $id_tipe_pertanyaan->id : 0;

      //if not exist then show 404 , make sure id is retrievable
      $id_tipe_pertanyaan = PertanyaanTipe::findOrFail($id_tipe_pertanyaan);
      $id_tipe_pertanyaan = $id_tipe_pertanyaan->id;

      /* Insert data ke table srv_grup */
      $t = new Grup;
      $t->nama = $request->input('judul');
      $t->id_skpd = $request->input('skpd');
      $t->id_tipe_pertanyaan = $id_tipe_pertanyaan;
      $t->tahun = $request->input('tahun');
      $t->slug = $request->input('slug');
      $t->created_at = date('Y-m-d H:i:s');
      $t->created_by = Auth::id();
      $t->updated_at = NULL;
      $t->updated_by = 0;
      $t->deleted_at = NULL;
      $t->deleted_by = 0;
      $t->is_deleted = 0;
      $t->save();

      /* Insert ke table srv_grup_bobot */
      foreach($request->input('bobot_start') AS $idx => $val){
        $t2 = new GrupBobot;
        $t2->id_grup = $t->id;
        $t2->bobot_start = $val;
        $t2->bobot_end = $request->input('bobot_end')[$idx];
        $t2->nama = $request->input('nama')[$idx];
        $t2->is_deleted = 0;
        $t2->save();
      }


      /* Insert ke table srv_grup_pertanyaan */
      if(!is_null($request->input('id_pertanyaan'))){
        foreach($request->input('id_pertanyaan') AS $i => $v){
          foreach($v as $idx => $val ){

            // get pertanyaan by guid
            $pertanyaan = Pertanyaan::where("is_deleted", 0)->where("guid", $val)->first();

            if(isset($pertanyaan->id)){
              $t3 = new GrupPertanyaan;
              $t3->id_grup = $t->id;
              $t3->id_pertanyaan = $pertanyaan->id;
              $t3->is_deleted = 0;
              $t3->save();
            }
          }
        }
      }

      /* Jika ada custom pertanyaan */
      if(!is_null($request->input('custom_pertanyaan'))){
        foreach($request->input('custom_pertanyaan') AS $idx => $row) {
          foreach ($row AS $i => $r) {
            if($r != '' || $r != null){
              $guid_unsur = $idx;
              $unsur_pertanyaan = PertanyaanUnsur::where('is_deleted', 0)->where('guid', $guid_unsur)->get()->first();

              DB::transaction(function() use($r, $request, $t, $unsur_pertanyaan){
                /* insert custom pertanyaan ke table srv_pertanyaan */
                $tcp = new Pertanyaan;
                $tcp->guid = Str::uuid();
                $tcp->id_tipe_pertanyaan = 2; // bintang
                $tcp->nama = $r;
                //$tcp->id_skpd = $request->input('skpd');
                $tcp->is_custom = 1;
                $tcp->id_pertanyaan_unsur = $unsur_pertanyaan->id;
                $tcp->created_at = date('Y-m-d H:i:s');
                $tcp->created_by = Auth::id();
                $tcp->save();

                foreach(get_default_jawaban() AS $idx => $val){
                  /* Proses insert ke table srv_jawaban */
                  $t2 = new Jawaban;
                  $t2->id_pertanyaan = $tcp->id;
                  $t2->pilihan = $val->label;
                  $t2->nama = $val->label;
                  $t2->bobot = $val->bobot;
                  $t2->is_deleted = 0;
                  $t2->save();
                }

                /* insert custom pertanyaan ke table srv_grup_pertanyaan */
                $tcp2 = new GrupPertanyaan;
                $tcp2->id_grup = $t->id;
                $tcp2->id_pertanyaan = $tcp->id;
                $tcp2->is_deleted = 0;
                $tcp2->save();
              });
            }
          }
        }
      }

      $request->session()->flash('message', "Data berhasil disimpan!");
      return redirect('/srv/grup_pertanyaan');

    }

    public function edit($id)
    {
      $logged_user = Auth::user();

      $skpd = get_skpd_by_login();
      $data = Grup::findOrFail($id);
      //$data = Grup::where('id', $id)->get();
      //echo $data; die();

      $pertanyaan_tipe = PertanyaanTipe::where('is_deleted', 0)->get();

      $query_bobot = DB::table('srv_grup AS g')
      ->select(DB::raw('g.nama AS judul, g.slug, gb.bobot_start, gb.bobot_end, gb.nama, p.id AS id_pertanyaan, p.nama AS pertanyaan, p.is_custom'))
      ->join('srv_grup_bobot AS gb', 'g.id', '=', 'gb.id_grup')
      ->join('srv_grup_pertanyaan AS gp', 'g.id', '=', 'gp.id_grup')
      ->join('srv_pertanyaan AS p', 'p.id', '=', 'gp.id_pertanyaan')
      ->where('g.is_deleted', 0)
      ->where('gb.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('p.is_deleted', 0)
      ->where('g.id', $id)
      ->groupBy('gb.id')
      ->get();

      $query_pertanyaan = DB::table('srv_grup AS g')
      ->select(DB::raw('g.nama AS judul,g.slug, gb.bobot_start, gb.bobot_end, gb.nama, p.id AS id_pertanyaan, p.nama AS pertanyaan, pu.nama AS pertanyaan_unsur, pu.guid AS pu_guid, p.guid, p.is_custom'))
      ->join('srv_grup_bobot AS gb', 'g.id', '=', 'gb.id_grup')
      ->join('srv_grup_pertanyaan AS gp', 'g.id', '=', 'gp.id_grup')
      ->join('srv_pertanyaan AS p', 'p.id', '=', 'gp.id_pertanyaan')
      ->join('srv_pertanyaan_unsur AS pu', 'pu.id', '=', 'p.id_pertanyaan_unsur')
      ->where('g.is_deleted', 0)
      ->where('gb.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('p.is_deleted', 0)
      ->where('g.id', $id)
      ->groupBy('p.id')
      ->orderBy('gp.id', 'ASC')
      ->get();

      $count_pertanyaan_by_unsur = DB::table('srv_grup AS g')
      ->select(DB::raw('COUNT(p.id) count,pu.guid'))
      ->join('srv_grup_pertanyaan AS gp', 'g.id', '=', 'gp.id_grup')
      ->join('srv_pertanyaan AS p', 'p.id', '=', 'gp.id_pertanyaan')
      ->join('srv_pertanyaan_unsur AS pu', 'pu.id', '=', 'p.id_pertanyaan_unsur')
      ->where('g.is_deleted', 0)
      ->where('gp.is_deleted', 0)
      ->where('p.is_deleted', 0)
      ->where('g.id', $id)
      ->groupBy('pu.id')
      ->orderBy('gp.id', 'ASC')
      ->get();

      $konfigurasi = Konfigurasi::first();

      $jml_pertanyaan = [];
      foreach($count_pertanyaan_by_unsur as $idx => $row){
        $jml_pertanyaan[$row->guid] = $row->count;
      }

      $pertanyaan_unsur = PertanyaanUnsur::where("is_deleted", 0)->get();

      return view('srv.grup_pertanyaan-form', [
        'skpd' => $skpd,
        'data' => $data,
        'judul' => $data->nama,
        'slug' => $data->slug,
        'query_bobot' => $query_bobot,
        'query_pertanyaan' => $query_pertanyaan,
        'jml_pertanyaan' => $jml_pertanyaan,
        'pertanyaan_tipe' => $pertanyaan_tipe,
        'pertanyaan_unsur' => $pertanyaan_unsur,
        'konfigurasi' => $konfigurasi,
        'penilaian' => Penilaian::get()
      ]);
    }

    public function update(Request $request, $id)
    {
      $logged_user = Auth::user();

      $validator = Validator::make($request->all(), [
        'skpd' => 'required',
        'judul' => 'required',
        'bobot_start.*' => 'required|numeric',
        'bobot_end.*' => 'required|numeric',
        'nama' => 'required', // ini adalah label Penilaian
        'id_pertanyaan.*' => 'required',
        'slug' => [
          'required',
          Rule::unique('srv_grup', 'slug')->where(function ($query) use ($id){
            return $query->where('is_deleted', 0)->where('id', '!=', $id);
          })
        ],
        'tahun' => 'required|numeric',
      ],[
        'skpd.required' => 'Unit Layanan harus diisi!',
        'judul.required' => 'Judul Survey harus diisi!',
        'bobot_start.required' => 'Range Nilai harus diisi!',
        'bobot_start.numeric' => 'Range Nilai harus diisi angka!',
        'bobot_end.required' => 'Range Nilai harus diisi!',
        'bobot_end.numeric' => 'Range Nilai harus diisi angka!',
        'nama.required' => 'Label harus diisi!',
        'id_pertanyaan.required' => 'Pertanyaan harus diisi!',
        'tahun.required' => 'Tahun harus diisi!',
        'tahun.numeric' => 'Tahun harus diisi angka!',
      ]);

      if($validator->fails()){
        return back()
              ->withErrors($validator)
              ->withInput();
      }

      $id_tipe_pertanyaan = PertanyaanTipe::where('guid', $request->input('tipe_pertanyaan'))->get()->first();

      //check if tipe pertanyaan is exist?
      //make sure id pertanyaan is exist
      $id_tipe_pertanyaan = isset($id_tipe_pertanyaan->id) ? $id_tipe_pertanyaan->id : 0;

      //if not exist then show 404 , make sure id is retrievable
      $id_tipe_pertanyaan = PertanyaanTipe::findOrFail($id_tipe_pertanyaan);
      $id_tipe_pertanyaan = $id_tipe_pertanyaan->id;

      /* Update data ke table srv_grup */
      $t = Grup::findOrFail($id);
      $t->nama = $request->input('judul');
      //$t->id_skpd = $request->input('skpd');
      $t->id_tipe_pertanyaan = $id_tipe_pertanyaan;
      $t->tahun = $request->input('tahun');
      $t->slug = $request->input('slug');
      $t->updated_at = date('Y-m-d H:i:s');
      $t->updated_by = Auth::id();
      $t->save();

      /* Soft delete table srv_grup_bobot */
      DB::table('srv_grup_bobot')
        ->where('id_grup', $id)
        ->update(['is_deleted' => 1]);

      /* Insert ke table srv_grup_bobot */
      foreach($request->input('bobot_start') AS $idx => $val){
        $t2 = new GrupBobot;
        $t2->id_grup = $t->id;
        $t2->bobot_start = $val;
        $t2->bobot_end = $request->input('bobot_end')[$idx];
        $t2->nama = $request->input('nama')[$idx];
        $t2->is_deleted = 0;
        $t2->save();
      }

      /* Soft delete table srv_grup_pertanyaan */
      DB::table('srv_grup_pertanyaan')
        ->where('id_grup', $id)
        ->update(['is_deleted' => 1]);

      /* Insert ke table srv_grup_pertanyaan */
      if(!is_null($request->input('id_pertanyaan'))){
        foreach($request->input('id_pertanyaan') AS $i => $v){
          foreach($v as $idx => $val ){
            // get pertanyaan by guid
            $pertanyaan = Pertanyaan::where("is_deleted", 0)->where("guid", $val)->first();

            if(isset($pertanyaan->id)){
              $t3 = new GrupPertanyaan;
              $t3->id_grup = $t->id;
              $t3->id_pertanyaan = $pertanyaan->id;
              $t3->is_deleted = 0;
              $t3->save();
            }
          }
        }
      }

      /* Jika ada custom pertanyaan */
      if(!is_null($request->input('custom_pertanyaan'))){
        foreach($request->input('custom_pertanyaan') AS $idx => $row) {
          foreach ($row AS $i => $r) {
            if($r != '' || $r != null){
              $guid_unsur = $idx;
              $unsur_pertanyaan = PertanyaanUnsur::where('is_deleted', 0)->where('guid', $guid_unsur)->get()->first();

              DB::transaction(function() use($r, $request, $t, $unsur_pertanyaan){
                /* insert custom pertanyaan ke table srv_pertanyaan */
                $tcp = new Pertanyaan;
                $tcp->guid = Str::uuid();
                $tcp->id_tipe_pertanyaan = 2; // bintang
                $tcp->nama = $r;
                $tcp->id_skpd = $request->input('skpd');
                $tcp->is_custom = 1;
                $tcp->id_pertanyaan_unsur = $unsur_pertanyaan->id;
                $tcp->created_at = date('Y-m-d H:i:s');
                $tcp->created_by = Auth::id();
                $tcp->save();

                foreach(get_default_jawaban() AS $idx => $val){
                  /* Proses insert ke table srv_jawaban */
                  $t2 = new Jawaban;
                  $t2->id_pertanyaan = $tcp->id;
                  $t2->pilihan = $val->label;
                  $t2->nama = $val->label;
                  $t2->bobot = $val->bobot;
                  $t2->is_deleted = 0;
                  $t2->save();
                }

                /* insert custom pertanyaan ke table srv_grup_pertanyaan */
                $tcp2 = new GrupPertanyaan;
                $tcp2->id_grup = $t->id;
                $tcp2->id_pertanyaan = $tcp->id;
                $tcp2->is_deleted = 0;
                $tcp2->save();
              });
            }
          }
        }
      }

      $request->session()->flash('message', "Data berhasil diubah!");
      return redirect("/srv/grup_pertanyaan/edit/{$id}");
    }

    public function destroy(Request $request, $id)
    {
      /* Soft delete srv_grup */
      DB::table('srv_grup')
        ->where('id', $id)
        ->update([
          'deleted_at' => date('Y-m-d H:i:s'),
          'deleted_by' => Auth::id(),
          'is_deleted' => 1
        ]);

      /* Soft delete srv_grup_bobot */
      DB::table('srv_grup_bobot')
        ->where('id_grup', $id)
        ->update(['is_deleted' => 1]);

      /* Soft delete srv_grup_pertanyaan */
      DB::table('srv_grup_pertanyaan')
        ->where('id_grup', $id)
        ->update(['is_deleted' => 1]);

        $request->session()->flash('message', "Data berhasil dihapus!");
        return redirect('/srv/grup_pertanyaan');
    }

    public function list_datatables_api()
    {
      $data = DB::table('srv_grup AS g')
      ->select(DB::raw('g.id, g.nama AS judul, s.name AS skpd, g.tahun, pt.nama AS tipe_pertanyaan'))
      ->join('mst_skpd AS s', 's.id', '=', 'g.id_skpd')
      ->join('srv_pertanyaan_tipe AS pt', 'g.id_tipe_pertanyaan', '=', 'pt.id')
      ->whereIn('s.id', $this->id_skpd_by_login())
      ->where('g.is_deleted', 0)
      ->orderBy('g.id', 'ASC')
      ->get();

      return Datatables::of($data)->make(true);
    }

    public function get_pertanyaan_random(Request $request, $guid_tipe_pertanyaan)
    {
      $id_tipe_pertanyaan = PertanyaanTipe::where('guid', $guid_tipe_pertanyaan)->get()->first();
      $id_tipe_pertanyaan = isset($id_tipe_pertanyaan->id) ? $id_tipe_pertanyaan->id : 0;

      $id_pertanyaan_unsur = PertanyaanUnsur::where('guid', $request->input('guid'))->get()->first();
      $id_pertanyaan_unsur = isset($id_pertanyaan_unsur->id) ? $id_pertanyaan_unsur->id : 0;
      $jml_pertanyaan = $request->input("jml_pertanyaan");

      $query = "";

      $i = 0;
      $skpd = $this->id_skpd_by_login();
      $skpd = implode(",", $skpd->toArray());

      //check if tipe pertanyaan is exist?
      //make sure id pertanyaan is exist
      $data = DB::table('srv_pertanyaan')
      ->select('guid', 'nama')
      ->where('is_deleted', 0)
      ->where("is_custom", 0)
      ->where('id_tipe_pertanyaan', $id_tipe_pertanyaan)
      ->where("id_pertanyaan_unsur", $id_pertanyaan_unsur)
      //->whereIn('id_skpd', $this->id_skpd_by_login())
      //->where('id_skpd', $request->input('skpd'))
      ->inRandomOrder()
      ->limit($jml_pertanyaan)
      ->get();

      return response()->json($data);
    }

    public function get_pertanyaan_select2(Request $request, $guid_tipe_pertanyaan)
    {

      $guid_pertanyaan_unsur = $request->input("guid");
      $id_tipe_pertanyaan = PertanyaanTipe::where('guid', $guid_tipe_pertanyaan)->get()->first();
      $id_pertanyaan_unsur = PertanyaanUnsur::where('guid', $guid_pertanyaan_unsur)->get()->first();

      //check if tipe pertanyaan is exist?
      //make sure id pertanyaan is exist
      $id_tipe_pertanyaan = isset($id_tipe_pertanyaan->id) ? $id_tipe_pertanyaan->id : 0;
      $id_pertanyaan_unsur = isset($id_pertanyaan_unsur->id) ? $id_pertanyaan_unsur->id : 0;

      $search = !is_null($request->input("search")) ? $request->input("search") : '';

      $data = DB::table('srv_pertanyaan')
      ->select('guid', 'nama')
      ->where('nama', 'like', '%' . $search . '%')
      ->where("is_custom", 0)
      ->where('is_deleted', 0)
      ->where('id_tipe_pertanyaan', $id_tipe_pertanyaan)
      ->where('id_pertanyaan_unsur', $id_pertanyaan_unsur)
      //->whereIn('id_skpd', $this->id_skpd_by_login())
      ->get();

      return $data;
    }


    public function penerbitan()
    {
      $konfigurasi = Konfigurasi::first();
      return view('srv.penerbitan-list',[
        'konfigurasi' => $konfigurasi
      ]);
    }

    public function penerbitan_datatables_api($status = 0)
    {
      $data = DB::table('srv_grup AS g')
      ->select(DB::raw('g.slug, g.nama AS judul, s.name AS skpd, g.tahun, pt.nama AS tipe_pertanyaan, g.is_publish'))
      ->join('mst_skpd AS s', 's.id', '=', 'g.id_skpd')
      ->join('srv_pertanyaan_tipe AS pt', 'g.id_tipe_pertanyaan', '=', 'pt.id')
      ->whereIn('s.id', $this->id_skpd_by_login())
      ->where('g.is_deleted', 0)
      ->where("g.is_publish", $status)
      ->orderBy('g.id', 'ASC')
      ->get();

      return Datatables::of($data)->make(true);
    }


    public function publish(Request $request,$slug, $status)
    {
      //find the grup by slug
      $grup = Grup::where("is_deleted", 0)->where("slug", $slug)->first();

      $grup = Grup::findOrFail($grup->id);
      $grup->is_publish = $status;
      $grup->save();


      $request->session()->flash('message', "Data berhasil diproses!");
      return redirect("/srv/penerbitan");
    }

    public function get_grup_by_id_skpd(Request $request){
      $data = Grup::where('is_deleted', 0)
      ->where('id_skpd', $request->input('skpd'));

      if(!is_null($request->input('tahun'))){

        $data = $data->where('tahun', $request->input('tahun'));
      }

      $data = $data->orderBy('id', 'ASC')
      ->get();

      return response()->json([
          'msg' => 'success',
          'data' => $data
      ]);
    }
}
