<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
  protected $table = "mst_pendidikan";
  protected $primaryKey = "id";
  public $timestamps = false;
}
