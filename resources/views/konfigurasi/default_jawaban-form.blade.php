@extends('layouts.app')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Default Jawaban</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<form class="form-horizontal" method="POST">
{{ csrf_field() }}
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <table border="0" width="100%" class="table table-striped field_wrapper">
          <tr>
            <th width="5%" class="text-center">Pilihan</th>
            <th width="80%">Label</th>
            <th width="10%">Bobot</th>
            <th width="5%"></th>
          </tr>
        </table>
        <button type="button" name="button" class="btn btn-info add_button">+ add more</button>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href='{{url('')}}/srv/pertanyaan' class="btn btn-primary" type="button">Cancel</a>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<script>
$(function(){
  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);
  
  var maxField = 10; //Input fields increment limitation
  var addButton = $('.add_button'); //Add button selector
  var wrapper = $('.field_wrapper'); //Input field wrapper
  var fieldHTML = '<tr>';
  fieldHTML += '<td><input type="text" required class="form-control" name="pilihan[]"></td>';
  fieldHTML += '<td><input type="text" required class="form-control" name="jawaban[]"></td>';
  fieldHTML += '<td><input type="text" required class="form-control number-format" name="bobot[]"></td>';
  fieldHTML += '<td><a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a></td>';

  fieldHTML += '</tr>';
  var is_edit = '<?=isset($data) ? "1" : "0"?>';
  var x = '<?=isset($x) ? "{$x}" : "1"?>';
  var alphabet = ['','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

  //Once add button is clicked
  $(addButton).click(function(){
    //Check maximum number of input fields
    if(x < maxField){
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); //Add field html
      //append dulu biar jadi inputan terakhir
      $('.field_wrapper').find('input[name="pilihan[]"]').last().val(alphabet[x]);
    }
  });

  //Once remove button is clicked
  $(wrapper).on('click', '.remove_button', function(e){
    e.preventDefault();
    $(this).parent('td').parent('tr').remove(); //Remove field html
    x--; //Decrement field counter
  });

});
</script>
@endsection
