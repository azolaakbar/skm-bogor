@extends('layouts.app')

@section('content')

<div class="page-title">
  <div class="title_left">
    <h3>Tentang SKM</h3>
  </div>
</div>
<div class="clearfix"></div>
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Tentang SKM</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form class="form-horizontal" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
              Tentang <span class="required">*</span> :
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea name='tentang' required="required" class="form-control col-md-7 col-xs-12" style="min-height: 400px">{{ !is_null(old('tentang')) ? old('tentang') : (isset($data->tentang) ? $data->tentang : '') }}</textarea>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$(function(){

});
</script>
@endsection
