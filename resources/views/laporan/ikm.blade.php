@extends('layouts.app')

@section('content')
<style>
i.fa {
   font-size: 5em;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>Laporan IKM</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<form class="form-horizontal" method="POST">
  {{ csrf_field() }}
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="form-group {{ must_show_skpd_form() ? '' : 'hide'}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            OPD <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='skpd' class="form-control select2" id="skpd">
              @foreach($skpd as $id => $row)

                @php
                  $selected = !is_null(old('skpd')) ? old('skpd') : (isset($request['skpd']) ? $request['skpd'] : '');
                  $selected = $selected == $row->id ? "selected" : "";
                @endphp

                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tahun <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tahun' class="form-control select2" id="tahun">
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</form>

@if(isset($request['skpd']))
<div class="row">
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Survey dan IKM terendah tahun {{$request['tahun']}}</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Berikut adalah urutan survey & IKM-nya berdasarkan score IKM, mulai dari yang terendah:
        <canvas id="ikm_asc"></canvas>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Survey dan IKM tertinggi tahun {{$request['tahun']}}</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Berikut adalah urutan survey & IKM-nya berdasarkan score IKM, mulai dari yang tertinggi:
        <canvas id="ikm_desc"></canvas>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('/vendors/Chart.js/dist/Chart.js') }}"></script>
<script src="{{ asset('/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<script>
$(function(){
  var ikm_asc = document.getElementById('ikm_asc').getContext('2d');
  var chart_1 = new Chart(ikm_asc, {
      type: 'horizontalBar',
      data: {
          labels: [
            @foreach($ikm_tahunan_asc AS $row)
            '{{$row->survey}}',
            @endforeach
          ],
          datasets: [{
            label: 'Total IKM',
            data: [
              @foreach($ikm_tahunan_asc AS $row)
              '{{$row->ikm}}',
              @endforeach
            ],
            backgroundColor: [
              @foreach($ikm_tahunan_asc AS $row)
              'rgba(54, 162, 235, 0.2)',
              @endforeach
            ],
            borderColor: [
              @foreach($ikm_tahunan_asc AS $row)
              'rgba(54, 162, 235, 1)',
              @endforeach
            ],
            borderWidth: 1
          }]
      },
      options:{
        scales: {
          xAxes : [{
            ticks : {
              max : 4,
              min : 0
            }
          }]
        }
      }
  });
  //
  var ikm_desc = document.getElementById('ikm_desc').getContext('2d');
  var chart_2 = new Chart(ikm_desc, {
      type: 'horizontalBar',
      data: {
          labels: [
            @foreach($ikm_tahunan_desc AS $row)
            '{{$row->survey}}',
            @endforeach
          ],
          datasets: [{
            label: 'Total IKM',
            data: [
              @foreach($ikm_tahunan_desc AS $row)
              '{{$row->ikm}}',
              @endforeach
            ],
            backgroundColor: [
              @foreach($ikm_tahunan_desc AS $row)
              'rgba(54, 162, 235, 0.2)',
              @endforeach
            ],
            borderColor: [
              @foreach($ikm_tahunan_desc AS $row)
              'rgba(54, 162, 235, 1)',
              @endforeach
            ],
            borderWidth: 1
          }]
      },
      options:{
        scales: {
          xAxes : [{
            ticks : {
              max : 4,
              min : 0
            }
          }]
        }
      }
  });
});
</script>
@endif
<script>
$(function(){

  // function get_survey(){
  //   $.post("/srv/grup_pertanyaan/get_grup_by_id_skpd", {"skpd" : $("#skpd").val(), "tahun": $("#tahun").val()},
  //   function(res){
  //     if(res.data != null){
  //       var option = '';
  //       var request = '{{isset($request["survey"]) ? $request["survey"] : ""}}';
  //
  //       $.each(res.data, function(idx, val){
  //         if(request != '' && request == val.id){
  //           var selected = 'selected';
  //         } else {
  //           var selected = '';
  //         }
  //         option += '<option value="'+ val.id +'" '+selected+'>'+ val.nama +'</option>';
  //         $(".bulan-cover").show();
  //       });
  //       $("#survey").html(option);
  //     }
  //   });
  //
  // }


  get_tahun();
  function get_tahun(){
    $(".bulan-cover").hide();
    $.get('{{url("")}}/laporan/srv_bulanan/get_tahun_select2/' + $('#skpd').val(), function(res){
      console.log(res);
      if(res != null){
        var option = '';
        var request = '{{isset($request["tahun"]) ? $request["tahun"] : ""}}';

        $.each(res, function(idx, val){
          if(request != '' && request == val.id){
            var selected = 'selected';
          } else {
            var selected = '';
          }
          option += '<option value="'+ val.tahun +'" '+ selected +'>'+ val.tahun +'</option>';
        });
        $("#tahun").html(option);
      }
    }).done(function(){
      //get_survey();
    });
  }

  $("#skpd").on("change", function(){
    get_tahun();
  });

  // $("#tahun").on("change", function(){
  //   get_survey();
  // });


});
</script>
@endsection
