@extends('layouts.app')

@section('content')
<style>
i.fa {
   font-size: 5em;
}
</style>
<div class="page-title">
  <div class="title_left">
    <h3>Laporan IKM Tingkat Kota</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif
<form class="form-horizontal" method="POST">
  {{ csrf_field() }}
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Filter</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tahun <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tahun' class="form-control select2" id="tahun">
              @foreach($option_tahun AS $row)
              @php
              $selected = isset($request['tahun']) && $request['tahun'] == $row->tahun ? 'selected' : '';
              @endphp
              <option value="{{$row->tahun}}" {{$selected}}>{{$row->tahun}}</option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</form>

@if(isset($request['tahun']))
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>IKM Tingkat Kota Tahun {{$request['tahun']}}</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <canvas id="ikm_asc"></canvas>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('/vendors/Chart.js/dist/Chart.js') }}"></script>
<script src="{{ asset('/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<script>
$(function(){
  var ikm_asc = document.getElementById('ikm_asc').getContext('2d');
  var chart_1 = new Chart(ikm_asc, {
      type: 'horizontalBar',
      data: {
          labels: [
            @foreach($ikm_tingkat_kota AS $row)
            '{{$row->skpd_name}}',
            @endforeach
          ],
          datasets: [{
            label: 'Total IKM',
            data: [
              @foreach($ikm_tingkat_kota AS $row)
              '{{$row->ikm}}',
              @endforeach
            ],
            backgroundColor: [
              @foreach($ikm_tingkat_kota AS $row)
              'rgba(54, 162, 235, 0.2)',
              @endforeach
            ],
            borderColor: [
              @foreach($ikm_tingkat_kota AS $row)
              'rgba(54, 162, 235, 1)',
              @endforeach
            ],
            borderWidth: 1
          }]
      },
      options:{
        scales: {
          xAxes : [{
            ticks : {
              max : 4,
              min : 0
            }
          }]
        }
      }
  });
});
</script>
@endif
<script>
$(function(){


});
</script>
@endsection
