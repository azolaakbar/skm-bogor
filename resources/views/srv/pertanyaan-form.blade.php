@extends('layouts.app')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Pertanyaan</h3>
  </div>
</div>
<div class="clearfix"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>&#8226; {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('message'))
  <p class="alert alert-success">{!! Session::get('message') !!}</p>
@endif

<style>
i.fa {
   font-size: 3em;
}
</style>

<form class="form-horizontal" method="POST">
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        {{ csrf_field() }}

        {{-- 
        <div class="form-group {{ must_show_skpd_form() ? '' : 'hide'}}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            OPD <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='skpd' class="form-control select2">
              @foreach($skpd as $id => $row)

                @php
                  $selected = !is_null(old('skpd')) ? old('skpd') : (isset($data) ? $data->first()->id_skpd : '');
                  $selected = $selected == $row->id ? "selected" : "";
                @endphp

                <option value="{{$row->id}}" {{$selected}}>{{$row->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        --}}

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Unsur Layanan <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='pertanyaan_unsur' class="form-control select2">
              @foreach($pertanyaan_unsur as $id => $row)

                @php
                  $selected = !is_null(old('pertanyaan_unsur')) ? old('pertanyaan_unsur') : (isset($data) ? $data->first()->id_pertanyaan_unsur : '');
                  $selected = $selected == $row->id ? "selected" : "";
                @endphp

                <option value="{{$row->id}}" {{$selected}}>{{$row->nama}}</option>
              @endforeach
            </select>
          </div>
        </div>


        <div class="form-group {{ isset($konfigurasi->default_pertanyaan_tipe_hide) && $konfigurasi->default_pertanyaan_tipe_hide == 1 ? 'hide' : '' }}" >
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Tipe Pertanyaan <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <select name='tipe_pertanyaan' id="tipe_pertanyaan" class="form-control select2">
              @foreach($pertanyaan_tipe as $idx => $row)
              @php
              $automatic_selected = isset($konfigurasi->default_pertanyaan_tipe_selected) ? $konfigurasi->default_pertanyaan_tipe_selected : 0;
              $selected_pertanyaan_tipe = !is_null(old('tipe_pertanyaan')) && old('tipe_pertanyaan') == $row->guid ? 'selected' : (isset($data) && $data->first()->id_tipe_pertanyaan == $row->id ? 'selected' : ($automatic_selected == $row->id ? 'selected' : ''));
              @endphp
                <option value="{{$row->guid}}" {{$selected_pertanyaan_tipe}}>{{$row->nama}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">
            Pertanyaan <span class="required">*</span> :
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="pertanyaan" rows="3" cols="80" class="form-control">{{ !is_null(old('pertanyaan')) ? old('pertanyaan') : (isset($data) ? $data->first()->pertanyaan : '') }}</textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row hide">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <table border="0" width="100%" class="table table-striped">
          <tr>
            <th width="5%" class="text-center">Pilihan</th>
            <th width="80%">Label</th>
            <th width="10%">Bobot</th>
            <th width="5%"></th>
          </tr>
          <tbody id="jawaban_wrapper">
            <?php
            $x = 0;
            if(is_array(old("pilihan"))){
              // dd(old("pilihan"));
              foreach(old("pilihan") as $idx => $row){
            ?>
                  <tr>
                    <td><input type="text" required class="form-control" name="pilihan[]" value="{{$row}}"></td>
                    <td><input type="text" required class="form-control" name="jawaban[]" value="{{old('jawaban')[$idx]}}"></td>
                    <td><input type="text" required class="form-control " name="bobot[]" value="{{old('bobot')[$idx]}}"></td>
                    <td>
                      <?= $idx > 0 ? '<a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a>' : ''; ?>
                    </td>
                  </tr>
            <?php
                $x++;
              }
            } else if(isset($data)){ // jika form edit
              foreach($data as $idx => $row){
                $x++;
            ?>
              <tr>
                <td><input type="{{$row->id_tipe_pertanyaan == '2' ? 'hidden' : 'text' }}" required class="form-control" name="pilihan[]" value="{{ !is_null(old('pilihan[]')) ? old('pilihan[]') : (isset($data) ? $row->pilihan : '') }}"></td>
                <td>
                    <input type="{{$row->id_tipe_pertanyaan == '2' ? 'hidden' : 'text' }}" required class="form-control" name="jawaban[]" value="{{ !is_null(old('jawaban[]')) ? old('jawaban[]') : (isset($data) ? $row->jawaban : '') }}">

                  @if($row->id_tipe_pertanyaan == '2')
                    {!! str_replace("*", "<i class='fa fa-star' style='color:#ffac28;'></i>&nbsp;", $row->jawaban) !!}
                  @endif
                </td>
                <td><input type="{{$row->id_tipe_pertanyaan == '2' ? 'hidden' : 'text' }}" required class="form-control " name="bobot[]" value="{{ !is_null(old('bobot[]')) ? old('bobot[]') : (isset($data) ? $row->bobot : '') }}">
                {{ $row->bobot }}</td>
                <td>
                  <?= $idx > 0 && $row->tipe_pertanyaan == "Pilihan Ganda" ? '<a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a>' : ''; ?>

                </td>
              </tr>
          <?php }} ?>
        </tbody>
        </table>
        <div id="addmore_button_wrapper"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href='{{url('')}}/srv/pertanyaan' class="btn btn-primary" type="button">Cancel</a>
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<!-- <i class='fa fa-star' style='color:#ffac28;'></i> -->
<script>
$(function(){
  setTimeout(function() {
    $(".alert-success").hide(1000);
  }, 3000);

  var is_edit = '<?=isset($data) ? "1" : "0"?>';
  var x = '<?=isset($x) ? "{$x}" : "1"?>';
  var alphabet = ['','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

  var maxField = 10; //Input fields increment limitation
  var fieldHTML = '<tr>';
  fieldHTML += '<td><input type="text" required class="form-control" name="pilihan[]"></td>';
  fieldHTML += '<td><input type="text" required class="form-control" name="jawaban[]"></td>';
  fieldHTML += '<td><input type="text" required class="form-control " name="bobot[]"></td>';
  fieldHTML += '<td><a href="javascript:void(0);" class="remove_button"><span class="fa fa-close"></span></a></td>';
  fieldHTML += '</tr>';

  //Once add button is clicked
  $(document).on('click','.add_button', function(){ // ieu naha dam??
    //Check maximum number of input fields
    if(x < maxField){
      x++; //Increment field counter
      $("#jawaban_wrapper").append(fieldHTML); //Add field html
      //append dulu biar jadi inputan terakhir
      $('#jawaban_wrapper').find('input[name="pilihan[]"]').last().val(alphabet[x]);
    }
  });

  //Once remove button is clicked
  $("#jawaban_wrapper").on('click', '.remove_button', function(e){
    e.preventDefault();
    $(this).parent('td').parent('tr').remove(); //Remove field html
    x--; //Decrement field counter
  });
  <?php

  if(!is_array(old("pilihan"))){
    echo "
      if(is_edit == 0){ // trigger function show_default_jawaban() jika membuka form add.
       show_default_jawaban();
     }";
   }
   ?>

  function show_default_jawaban(){
    var guid_tipe_pertanyaan = $("#tipe_pertanyaan").val();
    var show_addmore_button = "";
    var show_remove_tr_button = "";
    var is_hide = "hide";

    if($("#tipe_pertanyaan").find("option:selected").html() == "Pilihan Ganda"){
      show_addmore_button += "<button type='button' name='button' class='btn btn-info add_button'>+ add more</button>";// didinya nambahken button setelah jquery initialize
      show_remove_tr_button += "<a href='javascript:void(0);' class='remove_button'><span class='fa fa-close'></span></a>";
      is_hide = "";
    }

    $.get("{{url('')}}/konfigurasi/get_default_jawaban/" + guid_tipe_pertanyaan).done(function(res){
      //var data_length = Object.keys(res.data).length;
      var html_jawaban = '';
      $.each(res.data, function(idx, val){
        //console.log(idx,val);
        x++;
        var show_remove_tr_button_final = idx != 'A' && show_remove_tr_button != '' ? show_remove_tr_button : '';

        html_jawaban += "<tr>";
        html_jawaban += "<td><input type='text' required class='form-control "+is_hide+"' name='pilihan[]' value='"+ idx +"'></td>";
        html_jawaban += "<td>";
        html_jawaban += "<input type='text' required class='form-control "+is_hide+"' name='jawaban[]' value='"+ val.label +"'>";
        if(is_hide != ""){
          for(i=1; i<=val.label.length; i++){
            html_jawaban += "<i class='fa fa-star' style='color:#ffac28;'></i>&nbsp;";
          }
        }
        html_jawaban += "</td>";
        html_jawaban += "<td>";
        html_jawaban += "<input type='text' required class='form-control "+is_hide+"' name='bobot[]' value='"+ val.bobot +"'>";
        if(is_hide != ""){
          html_jawaban += "<b>"+ val.bobot +"</b>";
        }
        html_jawaban += "</td>";
        html_jawaban += "<td>"+ show_remove_tr_button_final +"</td>";
        html_jawaban += "</tr>";
      });

      $("#jawaban_wrapper").html(html_jawaban);
      $("#addmore_button_wrapper").html(show_addmore_button);
    },'json');
  }

  $("#tipe_pertanyaan").on("change", function(){
    show_default_jawaban();
  })

});
</script>
@endsection
